export const serialize = (object) => {
  let str = [];
  for (let key in object)
    if (object.hasOwnProperty(key)) {
      str.push(encodeURIComponent(key) + "=" + encodeURIComponent(object[key]));
    }
  return str.join("&");
}