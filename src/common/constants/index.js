export const INPUT_TYPE = {
	select: "select",
	text: "text",
};

export const ENGINE_LIST = [
	{
		value: "google",
		label: "Google",
	},
	{
		value: "bing",
		label: "Bing",
	},
	{
		value: "both",
		label: "Google & Bing",
	},
];
