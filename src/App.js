import SearcherController from "views/Searcher/Searcher.controller";
import "./styles/main.scss";

function App() {
  return (
    <SearcherController />
  );
}

export default App;
