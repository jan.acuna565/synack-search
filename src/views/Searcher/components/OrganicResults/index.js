import React from "react";
import PropTypes from "prop-types";
import TitleLink from "components/TitleLink";
import "./styles.scss";

const OrganicResults = (props) => {
	const { data } = props;
	return (
		<div className="organicResults">
			<div className="displayedLink">{data.displayed_link}</div>
			<TitleLink link={data.link} title={data.title}/>
			<div className="snippet">
				<span>{data.snippet}</span>
			</div>
		</div>
	);
};

OrganicResults.propTypes = {
	data: PropTypes.object.isRequired,
};

export default OrganicResults;
