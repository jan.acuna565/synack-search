import React from "react";
import PropTypes from "prop-types";
import { INPUT_TYPE } from "common/constants";
import Button from "components/Button";
import InputForm from "components/InputForm";
import OrganicResults from "../OrganicResults";
import RelatedQuestions from "../RelatedQuestions";
import RelatedSearches from "../RelatedSearches";
import "./styles.scss";

const SearchedMode = (props) => {
	const {
		options,
		engine,
		searchInput,
		searchReducer,
		handleTypeSearch,
		handleClickSearch,
		handleSelectOption,
		handleInputEnter,
	} = props;

	const { list, isLoadingList } = searchReducer;
	return (
		<div className="searchedModeContainer">
			<div className="headerEngine">
				<InputForm
					name="engine"
					type={INPUT_TYPE.select}
					options={options}
					onChange={(e) => handleSelectOption(e, true)}
					className="engineSelector"
					value={engine?.value}
				/>
				<hr />
				<h1>{engine?.label}</h1>
			</div>
			<div className="searcherInputContainer">
				<InputForm
					className="searcherInput"
					onChange={handleTypeSearch}
					value={searchInput}
					onKeyUp={handleInputEnter}
				/>
				<Button onClick={handleClickSearch}>Search</Button>
			</div>
			{isLoadingList ? (
				"LOADING ..."
			) : (
				<>
					<div className="sectionContainer">
						<div className="totalResults">
							{engine?.label === "bing"
								? `${list?.time_taken_displayed} seconds`
								: `About ${list?.total_results} results (
					${list?.time_taken_displayed}s)`}
						</div>
					</div>
					<div className="sectionContainer">
						{list?.organic_results?.map((result) => {
							return (
								<OrganicResults
									key={`${result.position} - ${result.title}`}
									data={result}
								/>
							);
						})}
					</div>
					{list?.related_questions && (
						<div className="sectionContainer">
							<RelatedQuestions data={list?.related_questions} />
						</div>
					)}
					{list?.related_searches && (
						<div className="sectionContainer">
							<RelatedSearches data={list?.related_searches} />
						</div>
					)}
				</>
			)}
		</div>
	);
};

SearchedMode.propTypes = {
	options: PropTypes.array.isRequired,
	engine: PropTypes.object.isRequired,
	searchInput: PropTypes.string.isRequired,
	handleSelectOption: PropTypes.func.isRequired,
	handleTypeSearch: PropTypes.func.isRequired,
	handleClickSearch: PropTypes.func.isRequired,
	handleInputEnter: PropTypes.func.isRequired,
	searchReducer: PropTypes.object.isRequired,
};

export default SearchedMode;
