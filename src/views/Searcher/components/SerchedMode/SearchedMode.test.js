/* eslint-disable testing-library/no-node-access */
import React from "react";
import ReactDOM from "react-dom";
import { cleanup } from "@testing-library/react";
import SearchedMode from "./index";

afterEach(cleanup);

describe("<SearchedMode />", () => {
	const props = {
		searchReducer: {
			list: [],
			isLoading: false,
		},
	};

	it("renders without crashing", () => {
		const div = document.createElement("div");
		ReactDOM.render(<SearchedMode {...props} />, div);
	});
});
