import React from "react";
import PropTypes from "prop-types";
import ButtonLink from "components/ButtonLink";
import "./styles.scss";

const RelatedSearches = (props) => {
	const { data } = props;
	
	return (
		<div className="relatedSearches">
			<h3>Related searches</h3>
			<div className="container">
				{data?.map((result) => {
					return (
						<ButtonLink
							key={result.query}
							link={result.link}
							query={result.query}
						/>
					);
				})}
			</div>
		</div>
	);
};

RelatedSearches.propTypes = {
	data: PropTypes.object.isRequired,
};

export default RelatedSearches;
