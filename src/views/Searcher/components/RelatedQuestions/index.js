import React from "react";
import PropTypes from "prop-types";
import Accordion from "components/Accordion";
import "./styles.scss";

const RelatedQuestions = (props) => {
	const { data } = props;
	return (
		<div className="relatedQuestions">
			<h3>Related Questions</h3>
			{data?.map((result, index) => {
				return <Accordion key={`${index} - ${result.snippet}`} data={result} />;
			})}
		</div>
	);
};

RelatedQuestions.propTypes = {
	data: PropTypes.object.isRequired,
};

export default RelatedQuestions;
