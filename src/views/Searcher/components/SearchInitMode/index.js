import React from "react";
import PropTypes from "prop-types";
import InputForm from "components/InputForm";
import Button from "components/Button";
import { INPUT_TYPE } from "common/constants/index";
import "./searchInitMode.scss";

const SearchInitMode = (props) => {
	const {
		options,
		engine,
		searchInput,
		handleSelectOption,
		handleTypeSearch,
		handleClickSearch,
		handleInputEnter,
	} = props;
	return (
		<div className="searcherContainer">
			<div className="searcherFlex">
				<InputForm
					name="engine"
					type={INPUT_TYPE.select}
					options={options}
					onChange={handleSelectOption}
					className="engineSelector"
					value={engine?.value}
				/>
				<h1>{engine?.label}</h1>
				<div className="searcherInputContainer">
					<InputForm
						className="searcherInput"
						onChange={handleTypeSearch}
						onKeyUp={handleInputEnter}
						value={searchInput}
					/>
					<Button onClick={handleClickSearch}>Search</Button>
				</div>
			</div>
		</div>
	);
};

SearchInitMode.propTypes = {
	options: PropTypes.array.isRequired,
	engine: PropTypes.object.isRequired,
	searchInput: PropTypes.string.isRequired,
	handleSelectOption: PropTypes.func.isRequired,
	handleTypeSearch: PropTypes.func.isRequired,
	handleClickSearch: PropTypes.func.isRequired,
	handleInputEnter: PropTypes.func.isRequired,
};

export default SearchInitMode;
