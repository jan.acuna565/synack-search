import React from "react";
import SearchInitMode from "./components/SearchInitMode/index";
import SearchedMode from "./components/SerchedMode/index";

const SearcherView = (props) => {
  const { isSearchedMode } = props;
  let render = <SearchInitMode {...props}/>

  if(isSearchedMode) {
    render = <SearchedMode { ...props } />
  }
	return (
		<div className="pageContainer">
			{render}
		</div>
	);
};

export default SearcherView;
