import { ENGINE_LIST } from "common/constants/index";
import { getSearchResults } from "redux/actions/search.actions";
import { useComplexState } from "hooks/helper.hook";

export const useSearcherHook = (externalHooks) => {
	const { dispatch, searchReducer } = externalHooks;
	const [searcherStates, setSearcherStates] = useComplexState({
		engine: ENGINE_LIST[0],
		searchInput: "",
		isSearchedMode: false,
	});

	const handleSelectOption = (e, isSubmitMode) => {
		const engineSelected = ENGINE_LIST.find(
			(item) => item.value === e.target.value
		);
		setSearcherStates({
			engine: engineSelected,
		});
		if (isSubmitMode) {
			handleClickSearch(engineSelected.value);
		}
	};

	const handleTypeSearch = (e) => {
		setSearcherStates({
			searchInput: e.target.value,
		});
	};

	const handleInputEnter = (e) => {
		if (e.key === "Enter" || e.keyCode === 13) {
			handleClickSearch();
		}
	};

	const handleClickSearch = (engineSelected) => {
		if (searcherStates.searchInput !== "") {
			const params = {
				engine:
					typeof engineSelected === "string" ? engineSelected : searcherStates.engine.value,
				q: searcherStates.searchInput,
				count: "30",
				api_key: process.env.REACT_APP_API_KEY,
			};
			if (searcherStates.engine.value === "google") {
				params.location = "Seattle-Tacoma,+WA,+Washington,+United+States";
				params.hl = "en";
				params.gl = "us";
				params.google_domain = "google.com";
				params.safe = "active";
			} else if (searcherStates.engine.value === "bing") {
				params.safeSearch = "strict";
			}

			dispatch(getSearchResults(params));
			if (!searcherStates.isSearchedMode) {
				setSearcherStates({
					isSearchedMode: true,
				});
			}
		}
	};

	return {
		options: ENGINE_LIST,
		engine: searcherStates.engine,
		isSearchedMode: searcherStates.isSearchedMode,
		searchInput: searcherStates.searchInput,
		searchReducer,
		handleSelectOption,
		handleTypeSearch,
		handleClickSearch,
		handleInputEnter,
	};
};
