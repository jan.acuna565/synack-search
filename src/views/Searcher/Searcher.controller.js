import React from "react";
import { useDispatch, useSelector } from "react-redux";
import SearcherView from "./Searcher.view";
import { useSearcherHook } from "./hooks/searcher.hook";

const SearcherController = () => {
	let dispatch = useDispatch();
	const searchReducer = useSelector(({ searchReducer }) => searchReducer);
	const searcherStates = useSearcherHook({dispatch, searchReducer});
	return <SearcherView { ...searcherStates } />;
};

export default SearcherController;
