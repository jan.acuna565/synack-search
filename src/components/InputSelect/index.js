import React from "react";
import PropTypes from "prop-types";
import "./styles.scss";

const InputSelect = (props) => {
	const { name, className, options, onChange, value } = props;
	return (
		<select className={`select ${className}`} onChange={onChange} value={value}>
			{options?.map((option) => {
				return (
					<option key={`${name}-${option.label}`} value={option.value}>
						{option.label}
					</option>
				);
			})}
		</select>
	);
};

InputSelect.propTypes = {
	name: PropTypes.string.isRequired,
	className: PropTypes.string.isRequired,
	options: PropTypes.array.isRequired,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.string.isRequired,
};

export default InputSelect;
