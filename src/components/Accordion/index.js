import React, { useRef } from "react";
import PropTypes from "prop-types";
import TitleLink from "components/TitleLink";
import Chevron from "./Chevron";

import "./styles.scss";
import { useComplexState } from "hooks/helper.hook";

const Accordion = (props) => {
	const { data } = props;
	const [accordionStates, setAccordionStates] = useComplexState({
		active: "",
		height: "0px",
		rotate: "accordionIcon",
	});

	const content = useRef(null);

	const toggleAccordion = () => {
		setAccordionStates({
			active: accordionStates.active === "" ? "active" : "",
			height:
				accordionStates.active === "active"
					? "0px"
					: `${content.current.scrollHeight}px`,
			rotate:
				accordionStates.active === "active"
					? "accordionIcon"
					: "accordionIcon rotate",
		});
	};

	return (
		<div className="accordionContainer">
			<button
				className={`accordion ${accordionStates.active}`}
				onClick={toggleAccordion}
			>
				<p>{data?.question}</p>
				<Chevron
					className={`${accordionStates.rotate}`}
					width={10}
					fill={"#fff"}
				/>
			</button>
			<div
				ref={content}
				style={{ maxHeight: `${accordionStates.height}` }}
				className="accordionContent"
			>
				<div
					className="accordionText"
					dangerouslySetInnerHTML={{ __html: data?.snippet }}
				/>
				<TitleLink link={data?.link} title={data?.title} />
			</div>
		</div>
	);
};

Accordion.propTypes = {
	data: PropTypes.object.isRequired,
};

export default Accordion;
