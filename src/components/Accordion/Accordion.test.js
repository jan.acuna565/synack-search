/* eslint-disable testing-library/no-node-access */
import React from "react";
import ReactDOM from "react-dom";
import { cleanup } from "@testing-library/react";
import Accordion from "./index";

afterEach(cleanup);

describe("<Accordion />", () => {
		const data = {
			question: "Question?",
			snippet: "snippet",
			link: "www.link.com",
			title: "This is the title",
		};
	it("renders without crashing", () => {
		const div = document.createElement("div");
		ReactDOM.render(<Accordion data={data} />, div);
	});

});
