/* eslint-disable testing-library/no-node-access */
import React from "react";
import ReactDOM from "react-dom";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import Button from "./index";

afterEach(cleanup);

describe("<Button />", () => {
  const children = "Search";
  const mockHandler = jest.fn();
  const setup = () => render(<Button onClick={ mockHandler }>{children}</Button>);

	it("renders without crashing", () => {
		const div = document.createElement("div");
		ReactDOM.render(<Button></Button>, div);
	});

	it("renders button text", () => {
    setup();
		expect(screen.getByText(children)).toHaveTextContent(children);
	});

	it("click on the button and handle ONCE", () => {
    setup();
		fireEvent.click(screen.getByText(children));
		expect(mockHandler).toHaveBeenCalledTimes(1);
	});
});
