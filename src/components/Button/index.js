import React from "react";
import PropTypes from "prop-types";
import "./styles.scss";

const Button = (props) => {
	const { children, type, onClick, className } = props;
	return (
		<button
			type={type}
			onClick={onClick}
			className={`button ${className || ""}`}
		>
			{children}
		</button>
	);
};

Button.propTypes = {
	onClick: PropTypes.func.isRequired,
	className: PropTypes.string,
	type: PropTypes.string,
	children: PropTypes.string,
};

Button.defaultProps = {
	children: "Search",
	type: "text",
	className: "",
};

export default Button;
