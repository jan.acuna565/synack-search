import React from "react";
import PropTypes from "prop-types";
import "./styles.scss";

const InputText = (props) => {
	const { type, name, onChange, className, value, onKeyUp } = props;
	return (
		<input
			type={type}
			name={name}
			onChange={onChange}
			className={`inputText ${className || ""}`}
			value={value}
			onKeyUp={onKeyUp}
			aria-label={name}
		/>
	);
};

InputText.propTypes = {
	type: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.string.isRequired,
	className: PropTypes.string.isRequired,
	onKeyUp: PropTypes.func.isRequired,
};


export default InputText;
