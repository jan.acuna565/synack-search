import React from "react";
import PropTypes from "prop-types";
import { INPUT_TYPE } from "common/constants";
import InputText from "components/InputText";
import InputSelect from "components/InputSelect";

const InputForm = (props) => {
	const { type } = props;

	const createInput = () => {
		switch (type) {
			case INPUT_TYPE.select:
				return <InputSelect {...props} />;

			case INPUT_TYPE.text:
				return <InputText {...props} />;
			default:
				return <InputText {...props} />;
		}
	};

	return createInput();
};

InputForm.propTypes = {
	type: PropTypes.string,
};

InputForm.defaultProps = {
	type: "text",
};

export default InputForm;
