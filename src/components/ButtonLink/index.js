import React from "react";
import PropTypes from "prop-types";
import "./styles.scss";

const ButtonLink = (props) => {
	const { link, query } = props;
	return (
		<a href={link} target="_blank" className="buttonLink" rel="noreferrer">
			{query}
		</a>
	);
};

ButtonLink.propTypes = {
	link: PropTypes.string.isRequired,
	query: PropTypes.string.isRequired,
};

export default ButtonLink;
