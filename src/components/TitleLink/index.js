import React from "react";
import PropTypes from "prop-types";
import "./styles.scss";

const TitleLink = (props) => {
  const { link, title } = props;
	return (
		<a href={link} target="_blank" className="titleLink" rel="noreferrer">
			<h3>{title}</h3>
		</a>
	);
};

TitleLink.propTypes = {
	link: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
};

export default TitleLink;
