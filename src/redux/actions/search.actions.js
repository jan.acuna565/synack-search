import { serialize } from "common/helpers";
import {
	GET_LIST,
	GET_LIST_LOADING,
	GET_LIST_ERROR,
} from "../actionTypes/search.actionTypes";

export const getSearchResults = (params) => async (dispatch) => {
	const { engine } = params;
	let formattedResponse = "";
	dispatch({
		type: GET_LIST_LOADING,
	});
	try {
		if (engine === "both") {
			// google
			const tempGoogleParams = params;
			tempGoogleParams.engine = "google";
			const urlGoogleParams = `${process.env.REACT_APP_SERVER}?${serialize(
				tempGoogleParams
			)}`;
			const responseGoogle = await fetch(urlGoogleParams, { params });
			const dataGoogle = await responseGoogle.json();
			//bing
			const tempBingParams = params;
			tempBingParams.engine = "bing";
			const urlBingParams = `${process.env.REACT_APP_SERVER}?${serialize(
				tempBingParams
			)}`;
			const responseBing = await fetch(urlBingParams, { params });
			const dataBing = await responseBing.json();
			formattedResponse = {
				total_results: dataGoogle.search_information.total_results,
				time_taken_displayed:
					dataGoogle.search_information.time_taken_displayed +
					dataBing.search_metadata.total_time_taken,
				organic_results: [
					...dataGoogle.organic_results,
					...dataBing.organic_results,
				],
				related_questions: dataGoogle.related_questions,
				related_searches: [
					...dataGoogle.related_searches,
					...dataBing.related_searches,
				],
			};
		} else {
			const url = `${process.env.REACT_APP_SERVER}?${serialize(params)}`;
			const response = await fetch(url, { params });
			const data = await response.json();
			if (engine === "google") {
				formattedResponse = {
					total_results: data.search_information.total_results,
					time_taken_displayed: data.search_information.time_taken_displayed,
					organic_results: data.organic_results,
					related_questions: data.related_questions,
					related_searches: data.related_searches,
				};
			} else if (engine === "bing") {
				formattedResponse = {
					total_results: null,
					time_taken_displayed: data.search_metadata.total_time_taken,
					organic_results: data.organic_results,
					related_questions: null,
					related_searches: data.related_searches,
				};
			}
		}

		dispatch({
			type: GET_LIST,
			payload: formattedResponse,
		});
	} catch (error) {
		dispatch({
			type: GET_LIST_ERROR,
			payload: error,
		});
		console.log("error", error);
	}
};
