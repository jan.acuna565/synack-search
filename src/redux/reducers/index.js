import { combineReducers } from "redux";
import searchReducer from "./search.reducer";

const rootReducer = combineReducers({
  searchReducer,
});

export default rootReducer;
