import reducer from "./search.reducer";

describe("Reducer SEARCH", () => {
	it('should have initial states ==> isLoadingList: false, list: null, errorList: ""', () => {
		const expected = { isLoadingList: false, errorList: "", list: null };
		const action = { type: "" };
		expect(reducer(undefined, action)).toEqual(expected);
	});

	it('should have states ==> isLoadingList: true, list: null, errorList: ""', () => {
		const expected = { isLoadingList: true, errorList: "", list: null };
		const action = { type: "GET_LIST_LOADING_SEARCH_TYPES" };
		expect(reducer(undefined, action)).toEqual(expected);
	});

	it('should have states ==> isLoadingList: false, list: [], errorList: ""', () => {
		const expected = { isLoadingList: false, errorList: "", list: [] };
		const action = { type: "GET_LIST_SEARCH_TYPES" };
		expect(reducer(undefined, action)).toEqual(expected);
	});
});
